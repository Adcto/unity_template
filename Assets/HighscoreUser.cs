﻿
public class HighscoreUser  {
    public string name;
    public int highscore;

    public HighscoreUser(string nombre, int score)
    {
        name = nombre;
        highscore = score;
    }

    public override string ToString()
    {
        return string.Format( "[User]");
    }
}
