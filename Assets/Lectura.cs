﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using SQLite4Unity3d;


public class Lectura : MonoBehaviour {

    private SQLiteConnection _connection;
	
	private FileStream archivo;
	// Use this for initialization
	void Start () {
        
		string path = Application.persistentDataPath + "/practica1";

        if (!File.Exists(path)) { 
            File.Copy(Application.streamingAssetsPath + "/practica1",path);
            Debug.Log("Copiando base de datos...");
        }

        Debug.Log(path);

        _connection = new SQLiteConnection(path, SQLiteOpenFlags.ReadWrite);

        var users = _connection.Table<usersAlternative>();
        var test = new usersAlternative();
        test.name = "test";
        test.highscore = 100;
        _connection.Insert(test);
        
        foreach(usersAlternative p in users){
            Debug.Log(p.ToString());
        }
		//archivo = new FileStream (path, FileMode.OpenOrCreate, FileAccess.ReadWrite,  FileShare.ReadWrite);


		//archivo.Close ();
	}

	

}
