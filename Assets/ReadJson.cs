﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class ReadJson : MonoBehaviour , ITestJason{

  
    private FileStream archivo;
    private List<HighscoreUser> users;
    // Use this for initialization
    void Start()
    {

        string path = Application.persistentDataPath + "/jsontest";

        if (!File.Exists(path))
        {
            File.Copy(Application.streamingAssetsPath + "/jsontest", path);
            Debug.Log("Copiando base de datos...");
        }

        Debug.Log(path);



        StreamReader file = new StreamReader(path);
        string JSONstring = file.ReadToEnd();
        JSONObject j = new JSONObject(JSONstring);
        readData(j);
        
    }

    void readData(JSONObject obj)
    {
        switch (obj.type)
        {
            case JSONObject.Type.OBJECT:
                for (int i = 0; i < obj.list.Count; i++)
                {
                    string key = (string)obj.keys[i];
                    JSONObject j = (JSONObject)obj.list[i];
                    Debug.Log(key);
                    readData(j);
                }
                break;
            case JSONObject.Type.ARRAY:
                foreach (JSONObject j in obj.list)
                {
                    readData(j);
                }
                break;
            case JSONObject.Type.STRING:
                Debug.Log(obj.str);
                break;
            case JSONObject.Type.NUMBER:
                Debug.Log(obj.n);
                break;

        }

    }


    public List<HighscoreUser> getHighscore()
    {
        return users;
    }

    public void addHighscore(string name, int score)
    {
        HighscoreUser newUser = new HighscoreUser(name,score);
        users.Add(newUser);
    }
}
