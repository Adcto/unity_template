﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ITestJason  {

    List<HighscoreUser> getHighscore();

    void addHighscore(string name, int score);

	
}
