﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestSerialization : MonoBehaviour {

    List<ShipValues> ships;

	// Use this for initialization
	void Start () {
        ships = new List<ShipValues>();
        for (int i = 1; i <= 3; i++) {
            ShipValues aux = new ShipValues();
            aux.posx = 10*i;
            aux.posy = 25 * i;
            aux.vx = 5 * i;
            aux.vy = 4 * i;
            aux.angle = 50*i;
            ships.Add(aux);
        
        }
        string serialization = StringSerializationAPI.Serialize(ships.GetType(), ships);
        Debug.Log("serializacion: " + serialization);

        List<ShipValues> deserialized = (List<ShipValues>)StringSerializationAPI.Deserialize(typeof(List<ShipValues>), serialization);
        for (int i = 0; i < deserialized.Count; i++ )
            Debug.Log("Deserialized nave " + i + ": " + deserialized[i].posx + " " + deserialized[i].posy + " " + deserialized[i].vx + " " + deserialized[i].vy + " " + deserialized[i].angle);
	
    }

}
