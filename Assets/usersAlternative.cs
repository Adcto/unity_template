﻿using SQLite4Unity3d;

public class usersAlternative  {

    [PrimaryKey, AutoIncrement]
	public int id { get; set; }
    public string name { get; set; }
    public int highscore { get; set; }

    public override string ToString()
    {
        return string.Format("[usersAlternative: Id={0}, Name={1},  Highscore={2}]", id, name,  highscore);
    }

}
